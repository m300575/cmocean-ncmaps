# Beautiful color maps for ncview

For some users ncview is still the first shot when looking at netCDF files.
Unfortunately, ncview does not provide a single color map that is of any use to
visualize data.

Therefore, this repository brings the beautiful
[cmocean](https://matplotlib.org/cmocean/) color maps to your nciew.

## Installation

Clone this repository and add the following line to your shell config:

```
export NCVIEWBASE=<path_to_this_repo>/ncmaps/
```

Please note, that ncview will append the new colormaps the list of available
options. To prevent you from cycling through the hell that are ncview's
built-in color maps each time you want to visualize data, you can disable all
other colormaps using the `Opts` button.
